#!/usr/bin/python3

import csv
import codecs

File = input('Enter CSV file name to normalize: ')


try:
    with codecs.open(File, encoding='utf-8') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        alldata = []
        for row in readCSV:
            alldata.append(row)
except Exception as e:
    with codecs.open(File, encoding='utf-8', errors='replace') as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        alldata = []
        for row in readCSV:
            alldata.append(row)
    print("Non UTF-8 characters are replaced.\n")


headers = alldata.pop(0)

def checkTimeStamp(data):
    timeStamp = data.split(' ')

    # Reorder date
    dateSplit = timeStamp[0].split('/')
    year = '20' + dateSplit[2]
    month = dateSplit[0]
    if len(month) < 2:
        month = '0' + month
    day = dateSplit[1]
    if len(day) < 2:
        day = '0' + day

    # Convert Time
    ampm = timeStamp[2]
    time = timeStamp[1].split(':')
    hour = int(time[0])

    # Convert to 24hr time
    if ampm == ('pm' or 'PM'):
        hour = hour + 12

    # Convert to UTC
    hour = hour + 7

    convertedTime = str(hour) + ':' + time[1] + ':' + time[2] + '-04:00'
    date = year + '-' + month + '-' + day
    convertedDateTime = date + 'T' + convertedTime
    return (convertedDateTime)


def checkZipCode(data):

    if len(data) < 5:
        while len(data) < 5:
            data = '0' + data
    elif len(data) > 5:
        data = data[0:4]
    return (data)

def checkFullName(data):
    data = data.upper()
    return (data)

def checkDuration(data):
    time = data.split(':')
    convertHoursToSeconds = float(time[0]) * 60 * 60
    convertMinutesToSeconds = float(time[1]) * 60
    totalSeconds = convertHoursToSeconds + convertMinutesToSeconds + float(time[2])
    return (totalSeconds)

def getTotalDuration(foo, bar):
    return (foo + bar)

for row in alldata:
    # Address & Note section are left alone
    row[0] = checkTimeStamp(row[0])
    row[2] = checkZipCode(row[2])
    row[3] = checkFullName(row[3])
    row[4] = checkDuration(row[4])
    row[5] = checkDuration(row[5])
    row[6] = getTotalDuration(row[4], row[5])
    
alldata.insert(0, headers)
print(alldata)
